/**
 * Created by Robert on 2017-06-09.
 */
public class Silnik {


    private double objetosc;
    private int moc;
    private boolean on;

    public Silnik(double objetosc, int moc){
        this.objetosc = objetosc;
        this.moc = moc;
    }

    public String Specification(){return "Objętość skokowa: " + objetosc + "\n" + moc + "KM"; }

    public void runVw() {
        on = true;
        System.out.println("Silnik został uruchomiony");
    }

    public void offVw(){
        on = false;
        System.out.println("Silnik został wyłączony");

    }
    public boolean isOn(){
        return on;
    }
}
